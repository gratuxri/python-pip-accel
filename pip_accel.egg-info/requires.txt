cached-property>=0.1.5
coloredlogs>=3.0
humanfriendly>=1.42
pip>=7.0,<7.2
setuptools>=7.0

[s3]
boto >= 2.32
